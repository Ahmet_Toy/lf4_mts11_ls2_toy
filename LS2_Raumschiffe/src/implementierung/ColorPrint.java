package implementierung;

public class ColorPrint {

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";

	public static void main(String[] args) {

		System.out.println(ANSI_GREEN + "Dieser Text ist gr�n");
		System.out.println("In einer neuen print-Anweisung wird weiter mit der ausgew�hlten Farbe geschrieben");
		System.out.println("geschrieben\nDaher muss nach dem Text die Konstant: ANSI_RESET hinzugef�gt" + ANSI_RESET);
		System.out.println(ANSI_RED + "Dieser Text ist Rot\n" + ANSI_RESET);

		StringBuilder s = new StringBuilder("Hallo");
		System.out.println(s);

		StringBuilder x = new StringBuilder();
		if (x.length() > 0) {
			System.out.println("Hier steht was drin.");
		} else {
			System.out.println("Dieser StringBuilder ist leer!");
		}
	}
}
