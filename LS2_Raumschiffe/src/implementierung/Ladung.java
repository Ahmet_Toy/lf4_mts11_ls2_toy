package implementierung;

/**
 * @author Ahmet Toy, MTS11
 * @version 1.0.
 */

public class Ladung {

  private String typ;
  private int anzahl;

  public Ladung() {
    // leerer, parameterloser (Standard-)Konstruktor
  }

  public Ladung(String typ, int anzahl) {

    this.typ = typ;
    this.anzahl = anzahl;
  }

  public void setTyp(String typ) {this.typ = typ;}

  public void setAnzahl(int anzahl) {this.anzahl = anzahl;}

  public String getTyp() {return this.typ;}

  public int getAnzahl() {return this.anzahl;}
}