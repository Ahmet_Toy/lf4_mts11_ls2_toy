package implementierung;

public class Kapitaen {

	private String name;
	private int steuerungSeit;

	public Kapitaen() {
		// leerer parameterloser (Standard-)Konstruktor
	}

	public Kapitaen(String name, int steuerungSeit) {
		this.name = name;
		this.steuerungSeit = steuerungSeit;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSteuerungSeit(int steuerungSeit) {
		this.steuerungSeit = steuerungSeit;
	}

	public String getName() {
		return this.name;
	}

	public int getSteuerungSeit() {
		return this.steuerungSeit;
	}
}
