package implementierung;

/**
 * @author Ahmet Toy, MTS11
 * @version 1.0.
 */

public class Raumschiff_Test {

	public static void main(String[] args) {
		/*
		 * Instanziierung der Objekte vom Typ Ladung
		 */
		Ladung ferengiSaft = new Ladung("Ferengi Schneckensaft", 200);

		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);

		Ladung roteMaterie = new Ladung("Rote Materie", 2);

		Ladung forschungssonde = new Ladung("Forschungssonde", 35);

		Ladung klingonenSchwert = new Ladung("Bat'leth Klingonen Schwert", 200);

		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);

		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);

		/*
		 * Instanziierung der Objekte vom Typ Raumschiff
		 */
		Raumschiff klingonen = new Raumschiff();
		klingonen.setName("IKS Hegh'ta");
		klingonen.setPhotonentorpedo(1);
		klingonen.setPhotonentorpedoGeladen(0);
		klingonen.setEnergieversorgung(100);
		klingonen.setSchutzschild(100);
		klingonen.setHuelle(100);
		klingonen.setLebenserhaltung(100);
		klingonen.setReperaturandroid(2);
		// Methodenaufrufe zum Hinzuf�gen von Ladungen:
		klingonen.addladung(ferengiSaft);
		klingonen.addladung(klingonenSchwert);

		Raumschiff romulaner = new Raumschiff();
		romulaner.setName("IRW Khazara");
		romulaner.setPhotonentorpedo(2);
		romulaner.setPhotonentorpedoGeladen(0);
		romulaner.setEnergieversorgung(100);
		romulaner.setSchutzschild(100);
		romulaner.setHuelle(100);
		romulaner.setLebenserhaltung(100);
		romulaner.setReperaturandroid(2);
		// Methodenaufrufe zum Hinzuf�gen von Ladungen:
		romulaner.addladung(borgSchrott);
		romulaner.addladung(roteMaterie);
		romulaner.addladung(plasmaWaffe);

		Raumschiff vulkanier = new Raumschiff();
		vulkanier.setName("Ni'Var");
		vulkanier.setPhotonentorpedo(0);
		vulkanier.setPhotonentorpedoGeladen(0);
		vulkanier.setEnergieversorgung(80);
		vulkanier.setSchutzschild(80);
		vulkanier.setHuelle(50);
		vulkanier.setLebenserhaltung(100);
		vulkanier.setReperaturandroid(5);
		// Methodenaufrufe zum Hinzuf�gen von Ladungen:
		vulkanier.addladung(forschungssonde);
		vulkanier.addladung(photonentorpedo);

		// Klingonen Torpedorohre beladen:
		System.out.println("\n>>Klingonen beladen ihre Torpedorohre mit 1 Photonentorpedo\n");
		klingonen.addladung(photonentorpedo);

		// Klingonen schie�t auf Romulaner mit Photonentorpedo:
		System.out.println(">>Klingonen schie�en auf Romulaner mit Photonentorpedo\n");
		klingonen.abschussPhotonentorpedo(romulaner);

		// Romulaner schie�t auf Klingonen mit Phaserkanone:
		System.out.println("\n>>Romulaner schi�en auf Klingonen mit Phaserkanonen\n");
		romulaner.abschussPhaserkanone(klingonen);

		// Vulkanier senden eine Nachricht:
		System.out.println("\n>>Vulkanier senden eine Nachricht\n");
		vulkanier.sendeNachricht("Gewalt ist nicht logisch");

		// Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr
		// Ladungsverzeichnis aus:
		klingonen.raumschiffZustand();
		klingonen.ladungsverzeichnisAusgeben();

		// Vulkanier setzen alle Reparaturandroiden f�r alle Schiffssysteme ein:
		vulkanier.androidEinsetzen(vulkanier.getReperaturandroid(), true, true, true, true);

		// Vulkanier verladen Photonentorpedo und r�umen ihr Ladungsverzeichnis auf:
		vulkanier.photonentorpedoLaden();
		vulkanier.updateLadungsverzeichnis(vulkanier.getLadungsverzeichnis());

		// Klingonen schie�en 2 weitere Photonentorpedos auf Romulaner:
		System.out.println("\n>>Klingonen schi�en 2 weitere Photonentorpedos auf Romulaner\n");
		klingonen.photonentorpedoLaden();
		klingonen.abschussPhotonentorpedo(romulaner);
		klingonen.photonentorpedoLaden();
		klingonen.abschussPhotonentorpedo(romulaner);

		// Ladungsverzeichnis und Zustand aller drei Schiffe ausgeben:
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.raumschiffZustand();

		romulaner.ladungsverzeichnisAusgeben();
		romulaner.raumschiffZustand();

		vulkanier.ladungsverzeichnisAusgeben();
		vulkanier.raumschiffZustand();
		System.out.println("\n");

		// Ausgabe des Broadcastkommunikators:
		System.out.print(Raumschiff.logbuchEintraege());

		Raumschiff.dialogAusgabe();
	}
}