package implementierung;

/**
 * @author Ahmet Toy, MTS11
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import java.util.Random;

public class Raumschiff {

	private String name;
	private double energieversorgung;
	private double schutzschild;
	private double lebenserhaltung;
	private double huelle;
	private int photonentorpedo;
	private int photonentorpedoGeladen;
	private int phaserkanone;
	private int reperaturandroid;
	private HashMap<String, Integer> ladungsverzeichnis = new HashMap<>();
	static ArrayList<String> broadcastKommunikator = new ArrayList<>();
	private static StringBuilder dialogReport = new StringBuilder("");

	public Raumschiff() {
		// Impliziter Konstruktor, der standardm��ig automatisch vorhanden ist.
	}

	public Raumschiff(String name, double energieversorgung, double schutzschild, double lebenserhaltung, double huelle,
			int photonentorpedo, int photonentorpedoGeladen, int phaserkanone, int reperaturandroid,
			HashMap<String, Integer> ladungsverzeichnis, ArrayList<String> broadcastKommunikator) {

		this.name = name;
		this.energieversorgung = energieversorgung;
		this.schutzschild = schutzschild;
		this.lebenserhaltung = lebenserhaltung;
		this.huelle = huelle;
		this.photonentorpedo = photonentorpedo;
		this.photonentorpedoGeladen = photonentorpedoGeladen;
		this.phaserkanone = phaserkanone;
		this.reperaturandroid = reperaturandroid;
		this.ladungsverzeichnis = ladungsverzeichnis;
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	/*
	 * Setter Methodem
	 */
	public void setName(String name) {
		this.name = name;
	}

	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	public void setSchutzschild(double schutzschild) {
		this.schutzschild = schutzschild;
	}

	public void setLebenserhaltung(double lebenserhaltung) {
		this.lebenserhaltung = lebenserhaltung;
	}

	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}

	public void setPhotonentorpedo(int photonentorpedo) {
		this.photonentorpedo = photonentorpedo;
	}

	public void setPhotonentorpedoGeladen(int photonentorpedoGeladen) {
		this.photonentorpedoGeladen = photonentorpedoGeladen;
	}

	public void setPhaserkanone(int phaserkanone) {
		this.phaserkanone = phaserkanone;
	}

	public void setReperaturandroid(int reperaturandroid) {
		this.reperaturandroid = reperaturandroid;
	}

	public void setLadungsverzeichnis(HashMap<String, Integer> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	/*
	 * Getter-Methoden
	 */
	public String getName() {
		return this.name;
	}

	public double getEnergieversorgung() {
		return this.energieversorgung;
	}

	public double getSchutzschild() {
		return this.schutzschild;
	}

	public double getLebenserhaltung() {
		return this.lebenserhaltung;
	}

	public double getHuelle() {
		return this.huelle;
	}

	public int getPhotonentorpedo() {
		return this.photonentorpedo;
	}

	public int getPhotonentorpedoGeladen() {
		return this.photonentorpedoGeladen;
	}

	public int getPhaserkanone() {
		return this.phaserkanone;
	}

	public int getReperaturandroid() {
		return this.reperaturandroid;
	}

	public HashMap<String, Integer> getLadungsverzeichnis() {
		return this.ladungsverzeichnis;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return Raumschiff.broadcastKommunikator;
	}

	// Implementierung der modellierten Methoden:

	/**
	 * F�gt dem Ladungsverzeichnis des aufrufenden Schiffes eine neue Ladung hinzu.
	 * 
	 * @param neueLadung: Erwartet ein Objekt vom Typ Ladung
	 */
	public void addladung(Ladung neueLadung) {
		HashMap<String, Integer> add = new HashMap<>();
		add = this.getLadungsverzeichnis();
		add.put(neueLadung.getTyp(), neueLadung.getAnzahl());
		this.setLadungsverzeichnis(add);

		if (neueLadung.getTyp().equals("Photonentorpedo")) {
			this.photonentorpedo = photonentorpedo + neueLadung.getAnzahl();
		}
	}

	/**
	 * Gibt den gesamten Zustand eines Raumschiffes in der Konsole aus.
	 */
	public void raumschiffZustand() {
		System.out.println(ColorPrint.ANSI_GREEN + "Zustandsbericht des Raumschiffs: " + this.getName() + ": \n"
				+ ColorPrint.ANSI_RESET);
		System.out.println("-Energieversorgung: " + this.getEnergieversorgung() + " %");
		System.out.println("-Schutzschild: " + this.getSchutzschild() + " %");
		System.out.println("-Lebenserhaltung: " + this.getLebenserhaltung() + " %");
		System.out.println("-H�lle: " + this.getHuelle() + " %");
	}

	/**
	 * Gibt das Ladungsverzeichnis eines Raumschiffes in der Konsole aus, in dem
	 * zuvor ein StringBuilder-Objekt erzeugt wird und die einzelnen
	 * Hash-Map-Elemente darin gespeichert werden.
	 */
	public void ladungsverzeichnisAusgeben() {
		StringBuilder ladung = new StringBuilder();

		for (String key : ladungsverzeichnis.keySet()) {
			ladung.append(key).append(": ").append(ladungsverzeichnis.get(key)).append("\n");
		}
		System.out.println(ColorPrint.ANSI_YELLOW + "\n--------Ladungsverzeichnis: " + this.getName() + " --------\n\n"
				+ ColorPrint.ANSI_RESET + ladung + ColorPrint.ANSI_YELLOW
				+ "\n-------------------------------------------\n" + ColorPrint.ANSI_RESET);
	}

	/**
	 * F�gt eine Nachricht der Array-List broadcastKommunikator als Element hinzu.
	 * Dabei werden die Strings farbig mithilfe der ColorPrint-Klasse hinzugef�gt.
	 * 
	 * @param nachricht: Erwartet einen String f�r die Nachricht.
	 */
	public void sendeNachricht(String nachricht) {
		Raumschiff.broadcastKommunikator.add(
				ColorPrint.ANSI_BLUE + "[*log/" + this.getName() + "]: " + ColorPrint.ANSI_RESET + nachricht + "\n");
	}

	/**
	 * Methode f�r die Ausgabe der Elemente in der ArrayList broadcastKommunikator.
	 * Dabei werden die Elemente �ber die Iteration der ArrayList einem
	 * StringBuilder-Objekt hinzugef�gt.
	 * 
	 * @return String aus lokalem StringBuilder-Objekt
	 */
	public static String logbuchEintraege() {

		StringBuilder logbuch = new StringBuilder();
		for (int i = 0; i < Raumschiff.broadcastKommunikator.size(); i++) {
			logbuch.append(broadcastKommunikator.get(i)).append("\n");
		}

		return ColorPrint.ANSI_PURPLE + "**** Eintr�ge im Broadcast-Kommunikator ****\n\n" + ColorPrint.ANSI_RESET
				+ logbuch + ColorPrint.ANSI_PURPLE + "\n**** Ende ****" + ColorPrint.ANSI_RESET;
	}

	/**
	 * Methode f�r das Speichern der logBuch-Eintr�ger in einen zus�tzlichen
	 * StringBuilder, der als �bergabe-Parameter f�r die Dialogausgabe ben�tigt
	 * wird.
	 * 
	 * @param nachricht: Erwartet einen String f�r die Nachricht.
	 */
	public void addToDialog(String nachricht) {
		dialogReport.append(nachricht).append("\n");
	}

	/**
	 * Voraussetzung: Die Attribute dialogReport vom Typ StringBuilder enth�lt
	 * String-Eintr�ge. Effekt: Das Dialogfenster mit JOptionPane wird ausgef�hrt
	 * und enth�lt die Eintr�ge. Ansonsten wird ein Dialog mit der Meldung, dass
	 * keine Eintr�ge vorhanden sind, ausgef�hrt.
	 */
	public static void dialogAusgabe() {
		if (dialogReport.length() > 0) {
			JOptionPane.showMessageDialog(null, Raumschiff.dialogReport);
		} else {
			JOptionPane.showMessageDialog(null, "Es sind keine Eintr�ge vorhanden!");
		}
	}

	/**
	 * Voraussetzung: Der Bestand an Photonentorpedos muss mind. 1 betragen und die
	 * Torpedorohre m�ssen leer sein. (Effekt:) Raumschiff entnimmt immer exakt 1
	 * Photonentorpedo aus dem Bestand und bel�dt die Torpedorohre (es kann immer
	 * nur 1 Torpedo je Aufruf geladen werden).
	 */
	public void photonentorpedoLaden() {
		if (this.photonentorpedo < 1) {
			addToDialog("[" + this.getName() + "]: "
					+ "Keine Torpedos gefunden! - Der Bestand an Photonentorpedos ist leer!");
			sendeNachricht("-=*click*=-");
			updateLadungsverzeichnis(this.ladungsverzeichnis);
		}
		if (this.getPhotonentorpedoGeladen() == 1) {
			addToDialog("[" + this.getName() + "]: " + "Nachladen nicht m�glich! - Im "
					+ "Torpedo-Abschussrohr befindet sich bereits eine Ladung!");
		} else {
			this.setPhotonentorpedoGeladen(1);
			this.setPhotonentorpedo(this.getPhotonentorpedo() - 1);
			System.out.print("Es wurde 1 Photonentorpedo im Raumschiff " + this.getName() + " eingesetzt\n");
			sendeNachricht("Es wurde 1 Photonentorpedo eingesetzt");
		}
	}

	/**
	 * Voraussetzung: Das Torpedorohr wurde zuvor mit 1 Torpedorohr beladen. Effekt:
	 * Abschuss eines Torpedos und Entladung des Torpedorohres (= 0). Andernfalls:
	 * Die Nachricht "Click" wird gesendet.
	 * 
	 * @param schiff: Erwartet eine Raumschiff-Instanz, die einen Photonentorpedo
	 *                abschie�en soll.
	 */
	public void abschussPhotonentorpedo(Raumschiff schiff) {
		if (this.getPhotonentorpedoGeladen() == 0) {
			sendeNachricht("-=*Click*=-");
			addToDialog("Abschuss nicht m�glich! Die Torpedorohre im Raumschiff: " + this.getName()
					+ " sind nicht geladen!\n");
			System.out.println("Abschuss nicht m�glich! \n Die Torpedorohre im Raumschiff: " + this.getName()
					+ " sind nicht geladen!");
			sendeNachricht("Abschuss nicht m�glich! Die Torpedorohre sind nicht geladen!");
		} else {
			this.setPhotonentorpedoGeladen(0);
			addToDialog("[" + this.getName() + "]: " + "Photonentorpedo erfolgreich abgeschossen!");
			sendeNachricht("Photonentorpedo abgeschossen");
		}
	}

	/**
	 * Voraussetzung: Die Energieversorgung eines Raumschiffs liegt bei mind. 50 %
	 * Effekt: Phaserkanone wird abgeschossen und Energieversorgung wird um 50 %
	 * verringert. Ansonsten: Die Nachricht "Click" wird versendet.
	 * 
	 * @param schiff: Erwartet eine Raumschiff-Instanz, die eine Phaserkanone
	 *                abschie�en soll.
	 */
	public void abschussPhaserkanone(Raumschiff schiff) {
		double reduzierungEnergieversorgung = this.getEnergieversorgung() * 0.5;

		if (this.getEnergieversorgung() < 50) {
			sendeNachricht("-=*Click*=-");
		} else {
			this.setEnergieversorgung(reduzierungEnergieversorgung);
			sendeNachricht("Phaserkanone abgeschossen");
			treffer(schiff);
		}
	}

	/**
	 * R�umt das Ladungsverzeichnis des aufrufenden Raumschiffs auf, in dem es das
	 * Element mit dem Key: Photonentorpedo l�scht.
	 * 
	 * @param ladungsverzeichnis: Erwartet das Ladungsverzeichnis vom Typ HashMap.
	 */
	public void updateLadungsverzeichnis(HashMap<String, Integer> ladungsverzeichnis) {

		ladungsverzeichnis.remove("Photonentorpedo");
	}

	/**
	 * Verringert zun�chst den Schutzschild des getroffenen Schiffes um 50
	 * Prozentpunkte und gibt in der Konsole die Meldung heraus, dass das Schiff
	 * getroffen wurde. Falls sich der Schutzschild des getroffenen Schiffes auf 0
	 * oder weniger verringert hat, wird die H�lle sowie die Energieversorgung des
	 * getroffenen Schiffes um 50 Prozentpunkte verringert. Falls sich die H�lle und
	 * die Energieversorgung auf 0 oder weniger verringert hat, wird die
	 * Lebenserhaltung des getroffenen Raumschiffes auf 0 gesetzt und das Raumschiff
	 * ist damit zerst�rt.
	 * 
	 * @param getroffenerSchiff: Erwartet eine Raumschiff-Instanz, das getroffen
	 *                           werden soll.
	 */
	public void treffer(Raumschiff getroffenerSchiff) {
		double schadenSchutzschild = getroffenerSchiff.getSchutzschild() - 50.0;
		double schadenEnergieversorgung = getroffenerSchiff.getEnergieversorgung() - 50.0;
		double schadenHuelle = getroffenerSchiff.getHuelle() - 50.0;

		getroffenerSchiff.setSchutzschild(schadenSchutzschild);
		System.out.println(getroffenerSchiff.getName() + " wurde getroffen!");

		if (getroffenerSchiff.getSchutzschild() <= 0.0) {
			getroffenerSchiff.setHuelle(schadenHuelle);
			getroffenerSchiff.setEnergieversorgung(schadenEnergieversorgung);
		}
		if ((getroffenerSchiff.getHuelle() <= 0.0) && (getroffenerSchiff.getEnergieversorgung() <= 0.0)) {
			getroffenerSchiff.setLebenserhaltung(0.0);
			sendeNachricht("Raumschiff: " + getroffenerSchiff.getName() + " wurde vollst�ndig zerst�rt.");
		}
	}

	/**
	 * Voraussetzung: Die Anzahl der vorhandenen Androiden muss mind. 1 betragen und
	 * f�r die 5 Schiffs- systeme muss jeweils entweder true oder false �bergeben
	 * werden. Effekt: Die Boolean-Werte werden in der ArrayList
	 * zuReparierendeSysteme gespeichert. Die ArrayList wird daraufhin zusammen mit
	 * der �bergebenen Anzahl an Androiden der erhoehungSchiffssystem-Methode
	 * �bergeben. Diese Methode liefert den Wert f�r die Erh�hung der jeweiligen
	 * Schiffssysteme zur�ck. In den if-Statements werden daraufhin die
	 * Erh�hungs-Werte den jeweiligen Schiffssystemen hinzuaddiert.
	 * 
	 * @param androiden:         Erwartet die Anzahl der einzusetzenden Androiden
	 * @param energieversorgung: Erwartet true, wenn f�r dieses Schiffssystem
	 *                           Androiden eingesetzt werden sollen oder false, wenn
	 *                           nicht.
	 * @param schutzschild:      Erwartet true, wenn f�r dieses Schiffssystem
	 *                           Androiden eingesetzt werden sollen oder false, wenn
	 *                           nicht.
	 * @param lebenserhaltung:   Erwartet true, wenn f�r dieses Schiffssystem
	 *                           Androiden eingesetzt werden sollen oder false, wenn
	 *                           nicht.
	 * @param huelle:            Erwartet true, wenn f�r dieses Schiffssystem
	 *                           Androiden eingesetzt werden sollen oder false, wenn
	 *                           nicht.
	 */
	public void androidEinsetzen(int androiden, boolean energieversorgung, boolean schutzschild,
			boolean lebenserhaltung, boolean huelle) {

		if (androiden < 1) {
			addToDialog("[" + this.getName() + "]: " + "Die Anzahl darf nicht 0 oder negativ sein!");
		}
		if (androiden > this.getReperaturandroid()) {
			addToDialog("[" + this.getName() + "]: " + "Die Anzahl ist h�her" + " als der aktuelle Bestand von: "
					+ this.getReperaturandroid() + " Androiden!\n" + "Die Anzahl wurde auf den Bestand herabgesetzt!");

			androiden = this.getReperaturandroid();
			this.setReperaturandroid(this.getReperaturandroid() - androiden);
		} else {
			this.setReperaturandroid(this.getReperaturandroid() - androiden);
		}

		ArrayList<Boolean> zuRepararierendeSysteme = new ArrayList<Boolean>();
		zuRepararierendeSysteme.add(energieversorgung);
		zuRepararierendeSysteme.add(schutzschild);
		zuRepararierendeSysteme.add(lebenserhaltung);
		zuRepararierendeSysteme.add(huelle);

		// Erh�hung der auf true gesetzten Schiffsstrukturen:
		double erhoehung = erhoehungSchiffssystem(androiden, zuRepararierendeSysteme);

		if (energieversorgung) {
			this.setEnergieversorgung(this.getEnergieversorgung() + erhoehung);
			addToDialog("[" + this.getName() + "]: " + "Die Energieversorgung wurde um " + erhoehung
					+ " % erh�ht\nDie Energieversorgung liegt jetzt bei: " + this.getEnergieversorgung() + " %\n");
			sendeNachricht("Die Energieversorgung wurde um " + erhoehung + " % erh�ht.");
		}
		if (schutzschild) {
			this.setSchutzschild(this.getSchutzschild() + erhoehung);
			addToDialog("[" + this.getName() + "]: " + "Der Schutzschild wurde um " + erhoehung + " % erh�ht!\n"
					+ "Der Schutzschild liegt jetzt bei: " + this.getSchutzschild() + " %\n");
			sendeNachricht("Der Schutzschild wurde um " + erhoehung + " % erh�ht.");
		}
		if (lebenserhaltung) {
			this.setLebenserhaltung(this.getLebenserhaltung() + erhoehung);
			addToDialog("[" + this.getName() + "]: " + "Die Lebenserhaltung wurde um " + erhoehung + " % erh�ht!\n"
					+ "Die Lebenserhaltung liegt jetzt bei: " + this.getLebenserhaltung() + " %\n");
			sendeNachricht("Die Lebenserhaltung wurde um " + erhoehung + " % erh�ht.");
		}
		if (huelle) {
			this.setHuelle(this.getHuelle() + erhoehung);
			addToDialog("[" + this.getName() + "]: " + "Die H�lle wurde um " + erhoehung + " % erh�ht!\n"
					+ "Die H�lle liegt jetzt bei: " + this.getHuelle() + " %\n");
			sendeNachricht("Die H�lle wurde um " + erhoehung + " % erh�ht.");
		}
	}

	/**
	 * Berechnung des Wertes f�r die Erh�hung eines Schiffssystems. Hierf�r wird
	 * eine mit Random generierte Zufallszahl verwendet und mit der Anzahl der
	 * �bergebenen Androiden multipliziert. Dieses Zwischen- ergebnis wird dann
	 * durch den R�ckgabewert der anzahlSchiffssysteme-Methode geteilt.
	 * 
	 * @param androiden:             Erwartet die Anzahl der in der
	 *                               androidEinsetzen-Methode �bergebenen Androiden.
	 * @param zuReparierendeSysteme: Erwartet die Boolean-ArrayList aus der
	 *                               androidEinsetzen-Methode, die daraufhin als
	 *                               Parameter innerhalb der Berechnung des returns
	 *                               an die anzahlSchiffssysteme-Methode
	 *                               weitergegeben wird.
	 * @return Wert der Erh�hung eines Schiffssystems (Explizite Typumwandlung in
	 *         einen Double).
	 */
	private double erhoehungSchiffssystem(int androiden, ArrayList<Boolean> zuReparierendeSysteme) {

		Random zZahl = new Random();
		int zufallsziffer = (zZahl.nextInt(100) + 1);

		// Berechnung und R�ckgabe des Wertes zur Erh�hung der Schiffssysteme:
		return (double) (zufallsziffer * androiden) / anzahlSchiffssysteme(zuReparierendeSysteme);
	}

	/**
	 * Iteriert �ber jedes Element der Boolean-ArrayList und erh�ht die Z�hlvariable
	 * anzahlSchiffs- systeme f�r jedes true-Element um 1.
	 * 
	 * @param zuReparierendeSysteme: Erwartet die Boolean-ArrayList, die von der
	 *                               erhoehungSchiffssystem- Methode weitergereicht
	 *                               wird.
	 * @return Die Anzahl der auf true gesetzten Schiffssysteme als Integer
	 */
	private int anzahlSchiffssysteme(ArrayList<Boolean> zuReparierendeSysteme) {
		int anzahlSchiffssysteme = 0;

		for (Boolean index : zuReparierendeSysteme) {
			if (index) { // By default: == true
				anzahlSchiffssysteme++;
			}
		}
		return anzahlSchiffssysteme;
	}
}
